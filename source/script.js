var INITIAL_MAX_HEIGHT = 16
var SCROLL_SPEED = 400

$(function () {
    $("article").each(function () {
        var $element = $(this)
        var scrollHeight = $element.actual('outerHeight') // Get total height

        $element.find('span').html('<img src="../images/icon-arrow-right-small.png"/>')
        $element.css({ "max-height": INITIAL_MAX_HEIGHT })
        $element.attr('internal-initialized', '1')

        $element.click(function (event) {
            event.stopPropagation()
            var height = $element.height()

            if (height <= INITIAL_MAX_HEIGHT) {
                $element.animate({ "max-height": scrollHeight }, SCROLL_SPEED, function () {
                    $element.find('span').html('<img src="../images/icon-arrow-down-small.png"/>')
                })
            } else {
                $element.animate({ "max-height": INITIAL_MAX_HEIGHT }, SCROLL_SPEED, function () {
                    $element.find('span').html('<img src="../images/icon-arrow-right-small.png"/>')
                })
            }
        })
    })
})

$('.expandable-genre > li').on('click', function (event) {
    event.stopPropagation()
    var $list = $(this).find('ul')
    // Expand or collapse the panel
    $list.slideToggle()
    $('.expandable-genre ul').not($list).slideUp(() => {
        $('.expandable-genre > li').not(this).removeClass('expanded')
        $(this).toggleClass('expanded')
    })
})

// Prevent list from closing when clicking on category item.
$('.expandable-genre > li li').on('click', function (event) {
    event.stopPropagation()
})

// Toggle checkbox when clicking on category item.
$(".expandable-genre > li li").on('click', function (event) {
    var checkbox = $(this).find(":checkbox")[0];
    if (event.target != checkbox) checkbox.checked = !checkbox.checked;
});